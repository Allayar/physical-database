CREATE DATABASE HealthData;
CREATE SCHEMA PublicSchemas;

CREATE TABLE PublicSchemas.Patient (
    patient_id SERIAL PRIMARY KEY NOT NULL,
    first_name VARCHAR(50) NOT null,
    last_name VARCHAR(50) NOT NULL,
    gender VARCHAR(10) CHECK (gender IN ('Male', 'Female', 'Other')),
    birthdate DATE NOT NULL CHECK (birthdate > '2000-01-01')
);

CREATE TABLE PublicSchemas.MedicalRecord (
    record_id SERIAL PRIMARY KEY NOT NULL,
    patient_id INT REFERENCES PublicSchemas.Patient(patient_id),
    measured_value NUMERIC NOT NULL CHECK (measured_value >= 0),
    record_date DATE NOT NULL CHECK (record_date > '2000-01-01')
);

INSERT INTO PublicSchemas.patient(first_name, last_name, gender, birthdate)
VALUES ('John', 'Doe', 'Male', '2000-02-15'),
    ('Jane', 'Smith', 'Female', '2001-04-30'),
    ('Alice', 'Johnson', 'Female', '2002-08-10'),
    ('Bob', 'Brown', 'Male', '2003-06-25'),
    ('Eva', 'White', 'Female', '2004-03-15'),
    ('Daniel', 'Wilson', 'Male', '2005-09-02'),
    ('Grace', 'Roberts', 'Female', '2006-11-18'),
    ('Michael', 'Davis', 'Male', '2007-07-29'),
    ('Olivia', 'Taylor', 'Female', '2008-05-05'),
    ('Sophia', 'Lee', 'Female', '2009-12-20'),
    ('Liam', 'Martin', 'Male', '2010-04-08'),
    ('Emma', 'Hall', 'Female', '2011-03-03'),
    ('Noah', 'Perez', 'Male', '2012-08-12'),
    ('Ava', 'Garcia', 'Female', '2013-09-25'),
    ('Mia', 'Martinez', 'Female', '2014-11-05'),
    ('James', 'Chen', 'Male', '2015-06-19'),
    ('William', 'Kim', 'Male', '2016-07-14'),
    ('Charlotte', 'Wang', 'Female', '2017-01-09'),
    ('Oliver', 'Liu', 'Male', '2018-10-22'),
    ('Isabella', 'Zhang', 'Female', '2019-09-14');
    
INSERT INTO PublicSchemas.MedicalRecord(patient_id, measured_value, record_date)
VALUES
    (1, 75.3, '2023-10-15'),
    (2, 62.8, '2023-09-20'),
    (1, 68.9, '2023-11-05'),
    (2, 71.2, '2023-08-22'),
    (3, 80.1, '2023-07-10'),
    (4, 66.5, '2023-06-28'),
    (5, 72.3, '2023-05-15'),
    (6, 76.8, '2023-04-02'),
    (7, 69.7, '2023-03-18'),
    (8, 67.4, '2023-02-09'),
    (9, 73.6, '2023-01-23'),
    (10, 79.2, '2022-12-10'),
    (3, 65.1, '2022-11-30'),
    (4, 74.5, '2022-10-25'),
    (5, 68.0, '2022-09-15'),
    (6, 70.9, '2022-08-07'),
    (7, 76.7, '2022-07-29'),
    (8, 71.3, '2022-06-14'),
    (9, 68.2, '2022-05-03'),
    (10, 72.6, '2022-04-18');
    
   
ALTER TABLE PublicSchemas.Patient
ADD COLUMN record_ts TIMESTAMP DEFAULT current_date;

ALTER TABLE PublicSchemas.MedicalRecord
ADD COLUMN record_ts TIMESTAMP DEFAULT current_date;


UPDATE PublicSchemas.Patient
SET record_ts = current_date
WHERE record_ts IS NULL;

UPDATE PublicSchemas.MedicalRecord
SET record_ts = current_date
WHERE record_ts IS NULL;